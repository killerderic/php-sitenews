<?php

/**
 * Simple file that defined our globals
 */

// DB
$dbInfo="mysql:host=localhost;dbname=nebulae";
$dbUser= "root";
$dbPassword="";

//Vues
$views['main']='view/main.php';
$views['admin']='view/admin.php';
$views['adminForm']='view/adminForm.php';
$views['error']='view/error.php';

// Others
$viewPerPagePath = "config/viewPerPage.txt";